{-# language StandaloneDeriving #-}
{-# language DeriveGeneric #-}
{-# language DeriveAnyClass #-}
{-# language OverloadedStrings #-}
module Errors where

import Data.Aeson
import Data.List.Split
import qualified ErrUtils as E
import GHC.Generics

deriving instance Show E.Severity

instance FromJSON E.Severity where
  parseJSON (String "SevOutput")      = pure E.SevOutput
  parseJSON (String "SevFatal")       = pure E.SevFatal
  parseJSON (String "SevInteractive") = pure E.SevInteractive
  parseJSON (String "SevDump")        = pure E.SevDump
  parseJSON (String "SevInfo")        = pure E.SevInfo
  parseJSON (String "SevWarning")     = pure E.SevWarning
  parseJSON (String "SevError")       = pure E.SevError

instance ToJSON E.Severity where
  toJSON E.SevOutput      = String "SevOutput"
  toJSON E.SevFatal       = String "SevFatal"
  toJSON E.SevInteractive = String "SevInteractive"
  toJSON E.SevDump        = String "SevDump"
  toJSON E.SevInfo        = String "SevInfo"
  toJSON E.SevWarning     = String "SevWarning"
  toJSON E.SevError       = String "SevError"

data Span = Span {
  file      :: String
, startLine :: Int
, startCol  :: Int
, endLine   :: Int
, endCol    :: Int
} deriving (Show, Generic, ToJSON, FromJSON)

data Error d = Error {
    span     :: Maybe Span
  , doc      :: d
  , severity :: E.Severity
  , reason   :: Maybe String
  } deriving (Show, Generic, ToJSON, FromJSON)

type JSONError = Error String

data DecodedMessage d = 
    SimpleMessage { simpleMessage :: String }
  | DecodedMessage {
      message :: d
    , context :: [String]
    , relevant_bindings :: Maybe String
    } deriving (Show, Generic, ToJSON, FromJSON)

decodeMessage :: String -> DecodedMessage String
decodeMessage msg
  = case splitOn "\\n\\u2022 " msg of
      [m]
        | take 5 m /= "\\u2022" -> SimpleMessage m
      [m]       -> DecodedMessage (clean m) [] Nothing
      [m, c]    -> DecodedMessage (clean m) (separate c) Nothing
      (m:c:r:_) -> DecodedMessage (clean m) (separate c) (Just (clean' r))
  where separate :: String -> [String]
        separate = splitOn "\\n  "
        clean :: String -> String
        clean = unwords . separate . drop 6
        clean' :: String -> String
        clean' = unlines . separate
        